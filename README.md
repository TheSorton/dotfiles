# dotfiles

These are my dotfiles.

# Wayland
I've switched to Wayland/Sway. A side effect of this is now this repo is pretty small. 

# Apps
* wayland
* sway
* dunst
* base16
* vim

# Screenshot
![sway](https://i.alan.moe/3b0f4.png)

#/bin/bash

grim -g "$(slurp)" ~/.tmp/sel.png

r=$( cat /dev/urandom | tr -cd 'a-f0-9' | head -c 5 )
i=$( echo "$r" )
cd ~/.tmp/ && 
mv "sel.png" "$i.png" && echo "$i"
rsync -a ~/.tmp/$r.png root@alan.moe:/root/http/img/ &&
echo "https://i.alan.moe/$i.png" | wl-copy && 
rm -r "$i.png"
notify-send 'Upload Successful!' -t 1000

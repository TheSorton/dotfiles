#/bin/bash

grim ~/.tmp/desk.png

r=$( cat /dev/urandom | tr -cd 'a-f0-9' | head -c 5 )
i=$( echo "$r" )
cd ~/.tmp/ &&
mv "desk.png" "$i.png" && echo "$i"
rsync -a ~/.tmp/$r.png $DROPSSH &&
echo "$URL/$i.png" | wl-copy &&
rm -r "$i.png"
notify-send 'Upload Successful!' -t 3000


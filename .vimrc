set nocompatible 
syntax enable
set tabstop=4
set softtabstop=4
set expandtab
set number
set showcmd
set cursorline
filetype indent on
set wildmenu
set lazyredraw
set showmatch
set termguicolors
set ruler
set incsearch
set laststatus=2
set autoread
set scrolloff=999 " keep cursor centered

colorscheme base16-default-dark

" Getting rid of bad habits
nnoremap <Insert> :echoe "Use i dumb fuck"<CR>

" maps
noremap <F2> :source /home/alan/.vimrc <CR> 
nnoremap ]t :tabn<cr>
nnoremap [t :tabp<cr>

" plug-vim
call plug#begin('~/.vim/plugged')

"Plugins
Plug 'tpope/vim-eunuch'

" Initialize plugin system
call plug#end()
